import fs from "fs";
import path from "path";
import ts from "typescript";
import {
  ArrowFunction,
  FunctionDeclaration,
  Identifier,
  MethodDeclaration,
  Node,
  Project,
  PropertyAssignment,
  PropertyDeclaration,
  VariableDeclaration,
} from "ts-morph";
import { CallerNode, Refs, Result } from "./@types/results.type";
import genMmd from "./gen-mmd";



function callerNode({
  node,
  identifier,
  type,
}: {
  node: Node<ts.Node>;
  identifier: Identifier;
  type: "method" | "function" | "arrowfunctions";
}) {
  return {
    pos: identifier.getPos(),
    end: identifier.getEnd(),
    name: identifier.getText(),
    type,
  };
}

function isMethodDeclarationWithIdentifier(
  node: Node<ts.Node>
): { node: MethodDeclaration; identifier: Identifier } | null {
  if (Node.isMethodDeclaration(node)) {
    const identifier = node.getFirstChildIfKind(ts.SyntaxKind.Identifier);
    if (identifier) {
      return {
        node,
        identifier,
      };
    }
  }

  return null;
}

function isFunctionDeclarationWithIdentifier(
  node: Node<ts.Node>
): { node: FunctionDeclaration; identifier: Identifier } | null {
  if (Node.isFunctionDeclaration(node)) {
    const childIdentifier = node.getFirstChildIfKind(ts.SyntaxKind.Identifier);
    if (childIdentifier) {
      return { node, identifier: childIdentifier };
    }

    const parent = node.getParent();
    if (
      parent &&
      (Node.isVariableDeclaration(parent) || Node.isPropertyAssignment(parent))
    ) {
      const identifier = node.getFirstChildIfKind(ts.SyntaxKind.Identifier);
      if (identifier) {
        return { node, identifier };
      }
    }
  }

  return null;
}

function isArrowFunctionWithIdentifier(node: Node<ts.Node>): {
  node: ArrowFunction;
  identifier: Identifier;
  parent: VariableDeclaration | PropertyDeclaration | PropertyAssignment;
} | null {
  if (Node.isArrowFunction(node)) {
    const parent = node.getParent();
    if (
      parent &&
      (Node.isVariableDeclaration(parent) ||
        Node.isPropertyAssignment(parent) ||
        Node.isPropertyDeclaration(parent))
    ) {
      const identifier = parent.getFirstChildIfKind(ts.SyntaxKind.Identifier);
      if (identifier) {
        return { node, identifier, parent };
      }
    }
  }

  return null;
}

function findCallerIdentifier({
  node,
  base,
}: {
  node: Node<ts.Node>;
  base: string;
}): CallerNode {
  const methodResult = isMethodDeclarationWithIdentifier(node);
  if (methodResult) {
    return callerNode({ ...methodResult, type: "method" });
  }

  const functionResult = isFunctionDeclarationWithIdentifier(node);
  if (functionResult) {
    return callerNode({ ...functionResult, type: "function" });
  }

  const arrowFunctionResult = isArrowFunctionWithIdentifier(node);
  if (arrowFunctionResult) {
    return callerNode({
      node: arrowFunctionResult.parent,
      identifier: arrowFunctionResult.identifier,
      type: "arrowfunctions",
    });
  }

  const parent = node.getParent();
  if (!parent) {
    if (Node.isSourceFile(node)) {
      return {
        pos: node.getPos(),
        end: node.getEnd(),
        name: toRelativePath({ absPath: node.getFilePath(), base }),
        type: "sourcefile",
      };
    }

    throw new Error("呼び出し元の定義を見つけることができませんでした。");
  }
  return findCallerIdentifier({ node: parent, base });
}

function walkReferences({
  node,
  base,
}: {
  node:
    | MethodDeclaration
    | FunctionDeclaration
    | VariableDeclaration
    | PropertyDeclaration
    | PropertyAssignment;
  base: string;
}) {
  const refs: Refs = [];
  try {
    node
    .findReferencesAsNodes()
    .filter((chNode) => {
      const parent = chNode.getParent();
      if (Node.isExportAssignment(parent) || Node.isImportClause(parent)) {
        return false;
      }
      return true;
    })
    .forEach((chNode) => {
      try {
        refs.push({
          filePath: toRelativePath({
            absPath: chNode.getSourceFile().getFilePath(),
            base,
          }),
          pos: chNode.getPos(),
          end: chNode.getEnd(),
          caller: findCallerIdentifier({ node: chNode, base }),
        });
      } catch (err) {
        // console.log({
        //   filePath: chNode.getSourceFile().getFilePath(),
        //   pos: chNode.getPos(),
        //   end: chNode.getEnd(),
        // });
        const parent = chNode.getParent();
        if (parent) {
          console.log(
            ts.SyntaxKind[parent.compilerNode.kind],
            parent.getText()
          );
        }
        console.log(
          ts.SyntaxKind[chNode.compilerNode.kind],
          chNode.getFullText()
        );
        console.log(err);
      }
    });
  } catch (err) {
    console.log('参照の取得に失敗しました。', node.getText())
  }

  return refs;
}


function walkAllChildren({
  node,
  depth,
  result,
  base,
}: {
  node: Node<ts.Node>;
  depth: number;
  result: Result;
  base: string;
}) {
  const methodResult = isMethodDeclarationWithIdentifier(node);
  const functionResult = isFunctionDeclarationWithIdentifier(node);
  const arrowFunctionResult = isArrowFunctionWithIdentifier(node);

  let refs: Refs;
  let name: string;
  if (methodResult) {
    name = methodResult.identifier.getText();
    refs = walkReferences({
      node: methodResult.node,
      base,
    });
    const filePath = toRelativePath({ absPath: node.getSourceFile().getFilePath(), base });
    if (!result[filePath]) {
      result[filePath] = [];
    }

    result[filePath].push({
      name,
      pos: node.getPos(),
      end: node.getEnd(),
      refs,
      type: 'method'
    });
  } else if (functionResult) {
    name = functionResult.identifier.getText();
    refs = walkReferences({
      node: functionResult.node,
      base,
    });
    const filePath = toRelativePath({ absPath: node.getSourceFile().getFilePath(), base });
    if (!result[filePath]) {
      result[filePath] = [];
    }

    result[filePath].push({
      name,
      pos: node.getPos(),
      end: node.getEnd(),
      refs,
      type: 'function'
    });
  } else if (arrowFunctionResult) {
    name = arrowFunctionResult.identifier.getText();
    refs = walkReferences({
      node: arrowFunctionResult.parent,
      base,
    });
    const filePath = toRelativePath({
      absPath: node.getSourceFile().getFilePath(),
      base,
    });
    if (!result[filePath]) {
      result[filePath] = [];
    }

    result[filePath].push({
      name,
      pos: node.getPos(),
      end: node.getEnd(),
      refs,
      type: 'arrowfunctions'
    });
  } else {
    name = "";
    refs = [];
  }

  depth++;
  node
    .getChildren()
    .forEach((c) => walkAllChildren({ node: c, depth, result, base }));
}

function toRelativePath({ absPath, base }: { absPath: string; base: string }) {
  return absPath.replace(base, "");
}

(() => {
  const tsConfigFilePath: string = process.argv[2];

  try {
    fs.statSync(tsConfigFilePath);
  } catch (err) {
    throw new Error(
      `${tsConfigFilePath} はファイルパス形式ではないか、もしくは存在しないファイルです。`
    );
  }

  const configBase = path.resolve(path.parse(tsConfigFilePath).dir);

  console.log(tsConfigFilePath, configBase);

  const project = new Project({
    tsConfigFilePath,
  });

  const result: Result = {};
  project
    .getSourceFiles()
    .forEach((sf) =>
      sf.forEachChild((ch) =>
        walkAllChildren({ node: ch, depth: 0, result, base: configBase })
      )
    );
  

  const mmdText = genMmd(result);

  fs.writeFileSync('result.mmd', mmdText, { encoding: 'utf-8' });
})();
