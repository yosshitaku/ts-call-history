export type Refs = {
  filePath: string;
  pos: number;
  end: number;
  caller: CallerNode;
}[];

export type Result = {
  [filePath: string]: (CallerNode & {
    refs: Refs;
  })[];
};

export type CallerNode = {
  pos: number;
  end: number;
  name: string;
  type: "sourcefile" | "method" | "function" | "arrowfunctions";
};
