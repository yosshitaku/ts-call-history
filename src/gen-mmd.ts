import { CallerNode, Result } from "./@types/results.type";

function mmdItemId({ pos, end, name, key }: CallerNode & { key: string }) {
  return `${key}_${pos}_${end}_${name}`;
}

function allFiles(result: Result) {
  const all: {
    [filePath: string]: CallerNode[];
  } = {};
  const fileMap: { [filePath: string]: string } = {};

  let key = 0;
  for (const filePath of Object.keys(result)) {
    if (!all[filePath]) {
      all[filePath] = [];
      fileMap[filePath] = key.toString();
      key++;
    }

    for (const func of result[filePath]) {
      const item = all[filePath].findIndex((f) => {
        return f.name === func.name && f.pos === func.pos && f.end === func.end;
      });

      if (item === -1) {
        all[filePath].push(func);
      }

      for (const ref of func.refs) {
        if (!all[ref.filePath]) {
          all[ref.filePath] = [];
          fileMap[ref.filePath] = key.toString();
          key++;
        }

        const refItem = all[ref.filePath].findIndex((f) => {
          return (
            f.name === ref.caller.name &&
            f.pos === ref.caller.pos &&
            f.end === ref.caller.end
          );
        });

        if (refItem === -1) {
          all[ref.filePath].push(ref.caller);
        }
      }
    }
  }

  return { all, fileMap };
}

function genMmd(result: Result) {
  const mmdTextLines: string[] = ["flowchart LR"];
  const { all, fileMap } = allFiles(result);

  for (const filePath of Object.keys(all)) {
    mmdTextLines.push(`\tsubgraph ${filePath}`);

    for (const func of all[filePath]) {
      let name = func.name;
      if (func.type === "sourcefile" && name.startsWith("/")) {
        name = name.replace("/", "");
      }
      mmdTextLines.push(
        `\t\t${mmdItemId({ ...func, key: fileMap[filePath] })}[${name}]`
      );
    }

    mmdTextLines.push(`\tend`);
  }

  for (const filePath of Object.keys(result)) {
    for (const func of result[filePath]) {
      const meId = mmdItemId({ ...func, key: fileMap[filePath] });
      for (const ref of func.refs) {
        mmdTextLines.push(
          `\t${mmdItemId({
            ...ref.caller,
            key: fileMap[ref.filePath],
          })}-->${meId}`
        );
      }
    }
  }

  return mmdTextLines.join("\n");
}

export default genMmd;
